﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurntPancakes
{
    static class Display
    {
        public static void display(GameState g) //should take a game state
        {
            //print to console for now
            for(int i = 0; i < g.Pancakes.Length; i++)
            {
                Console.WriteLine("size: " + g.Pancakes[i].Item1 + "\torientation: " + (g.Pancakes[i].Item2 == Orientation.Up ? "up" : "down") );
            }
            Console.WriteLine();
        }
    }
}
