﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace BurntPancakes
{
    // we want all the pancakes to be oriented Up
    enum Orientation { Up, Down };

    class GameState : IComparable<GameState>
    {
        public uint gameSize { get; }

        public GameState(uint numPancakes)
        {
            pancakes = new Tuple<uint, Orientation>[numPancakes];
            for (uint i = 0; i < pancakes.Length; i++)
            {
                pancakes[i] = Tuple.Create<uint, Orientation>(i + 1, Orientation.Up);
            }
            cost = 0;
<<<<<<< HEAD
            gameSize = numPancakes;
=======
            rand = new Random();
>>>>>>> e77bfdb101600280b24c9a27a493618be77c0c94
        }

        public GameState(GameState other)
        {
            pancakes = new Tuple<uint, Orientation>[other.pancakes.Length];
            Array.Copy(other.pancakes,pancakes,pancakes.Length);
            cost = other.cost;
            previousState = other.previousState;
            gameSize = other.gameSize;
        }

        // shuffle contents and randomly change orientation
        public void shuffle()
        {
            ArrayList itemsToShuffle = new ArrayList(pancakes.Length); //keeps track of items that have not yet been shuffled
            for (uint i = 0; i < pancakes.Length; i++)
            {
                itemsToShuffle.Add(i+1); //add all pancake sizes
            }

            for(int i = 0; i < pancakes.Length; i++)
            {
                int randIndex = rand.Next(itemsToShuffle.Count);
                Orientation randOrient = (rand.Next(2) == 1) ? Orientation.Up : Orientation.Down;
                pancakes[i] = new Tuple<uint, Orientation>((uint) itemsToShuffle[randIndex], randOrient);
                itemsToShuffle.RemoveAt(randIndex); //remove the pancake we just added from the list of sizes to be added
            }
        }

        public int Heuristic
        {
            get
            {
                int heuristic = 0;
                Orientation lastOrientation = Orientation.Up;
                uint lastPancakeSize = (uint) pancakes.Length + 1;
                for(int i = pancakes.Length - 1; i >= 0; i--) //going from the bottom of the pancake stack
                {
                    if(pancakes[i].Item2!=lastOrientation) // flips orientation
                    {
                        heuristic++;
                        //Console.WriteLine("flipped orientation");          
                    }
                    else if(pancakes[i].Item2 == Orientation.Up && pancakes[i].Item1 != lastPancakeSize - 1) //facing up, incorrect order
                    {
                        heuristic++;
                        //Console.WriteLine("incorrect order facing up: last pancake size - " + lastPancakeSize + "  this pancake size - " + pancakes[i].Item1);
                    }
                    else if (pancakes[i].Item2 == Orientation.Down && pancakes[i].Item1 != lastPancakeSize + 1) //facing down, incorrect order
                    {
                        heuristic++;
                        //Console.WriteLine("incorrect order facing down");
                    }
                    lastOrientation = pancakes[i].Item2;
                    lastPancakeSize = pancakes[i].Item1;
                }
                return heuristic; 
            }
        }
        

        // invert first num pancakes in the stack
        public void flip(int num)
        {
            Array.Reverse(pancakes, 0, num);
            for(int i = 0; i < num; i++)
            {
                //flip orientation of pancake:
                pancakes[i] = (pancakes[i].Item2 == Orientation.Up) ? new Tuple<uint, Orientation>(pancakes[i].Item1, Orientation.Down) : new Tuple<uint, Orientation>(pancakes[i].Item1, Orientation.Up);

            }
        }

        private int cost;
        public int Cost
        {
            get { return cost; }
            set { cost = value; }
        }

        private Tuple<uint, Orientation>[] pancakes;
        public Tuple<uint, Orientation>[] Pancakes
        {
            get { return pancakes; }
        }

        public bool Equals(GameState gs)
        {
            return Pancakes.Equals(gs.Pancakes);
        }

        public int CompareTo(GameState other)
        {
            if (Cost + Heuristic < other.Cost + other.Heuristic)
                return -1;
            else if (Cost + Heuristic > other.Cost + other.Heuristic)
                return 1;
            else
                return 0;
        }

        private GameState previousState;
        public GameState PreviousState
        {
            get { return previousState; }
            set { previousState = value; }
        }

        public List<GameState> getPath()
        {
            List<GameState> path = new List<GameState>();

            if (previousState != null)
            {
                path = previousState.getPath();
            }
            path.Add(this);

            return path;
        }
        private Random rand;
        public Random Rand
        {
            set { rand = value; }
        }
    }
}
