﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurntPancakes
{
    //game state struct containing tuples of pancake order and orientation
    //must be outside of class so struct name is recognized by display class and main as well?
    class Solver
    {
        public Solver(GameState init)
        {
            cur = init;
            visited = new HashSet<GameState>();
            queue = new List<GameState>();
            isSolved = false;
            n_visited = 0;
        }
        //private int heuristic(gameState)

        public GameState Solve()
        {
            while (cur.Heuristic != 0)
            {
                cur = nextState();
                n_visited++;
            }
            return cur;
        }

        //public A*
        public GameState nextState()
        {
            visited.Add(cur);
            List<GameState> neighbors = findNeighbors(cur);

            foreach (GameState node in neighbors)
            {
                if (!visited.Contains(node)) // needs to be fixed, since cost
                {
                    node.PreviousState = cur;
                    queue.Add(node);
                }
            }


            if (queue.Count == 0)
                return null;

            queue.Sort();
            do
            {
                cur = queue.First();
                queue.RemoveAt(0);
            } while (visited.Contains(cur) && queue.Count > 0);

            if (visited.Contains(cur))
                return null;

            return cur;
        }

        private List<GameState> findNeighbors(GameState gs)
        {
            List<GameState> adj = new List<GameState>();
            for (int i = 1; i <= gs.Pancakes.Length; i++)
            {
                GameState copy = new GameState(gs);
                copy.flip(i);
                copy.Cost += 1;
                adj.Add(copy);
            }
            return adj;
        }

        private GameState cur;
        private List<GameState> queue;
        private HashSet<GameState> visited;

        private bool isSolved;
        public bool IsSolved
        {
            get { return isSolved; }
        }
        private int n_visited;
        public int StatesVisited
        {
            get { return n_visited; }
        }
    }

}
