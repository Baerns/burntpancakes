﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurntPancakes {
    class Program {
        static void Main(string[] args) {
            //make an instance of solver
            //while solver is not solved,
            //  runs a step of solver
            //  displays
            const int n_trials = 1;
            Random rand = new Random();
            for (int i = 0; i < n_trials; i++)
            {
                //Console.WriteLine(i);
                GameState game = new GameState(5); // test
                game.Rand = rand;
                game.shuffle();
                Solver solver = new Solver(game);

                //Display.display(gs);
                //int steps = 0;
                game = solver.Solve();
                List<GameState> solnPath = game.getPath();
                foreach (GameState g in solnPath)
                    Display.display(g);


                Console.WriteLine("Solution path length: " + solnPath.Count + "\nStates visited: " + solver.StatesVisited);
            }
            Console.ReadLine();
        }
    }
}
